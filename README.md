# Create: Volts
## Goals
- Balanced Gameplay
- One way to do a thing
- Easy to understand for non-experienced (take a look at ftb university?)
- As many mods as needed, as few as possible
- Improve upon the vanilla experience and expand into tech
- Progression system
- A bit more exploration with a Vanilla+ feel

## Non-Goals
- Provide "something for everyone"
- Kitchen sink
