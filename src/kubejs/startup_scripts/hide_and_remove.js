// priority: 0

const hide_and_remove_json = JsonIO.read("kubejs/config/hide_and_remove.json")
global.hide_and_remove = {
    items: [],
    fluids: [],
}

/**
 *
 * @param {any} json
 * @param {Array<string>} parsed
 */
function parse_hide_and_remove_list(json, parsed) {
    console.log(json)
    for (const mod in json) {
        console.log(mod)
        let ids = json[mod]
        console.log(ids)
        ids.forEach((id) => parsed.push(`${mod}:${id}`))
    }
}

parse_hide_and_remove_list(hide_and_remove_json["items"], global.hide_and_remove.items)
parse_hide_and_remove_list(hide_and_remove_json["fluids"], global.hide_and_remove.fluids)
