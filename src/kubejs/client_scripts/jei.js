// priority: 0

JEIEvents.hideItems((e) => {
    global.hide_and_remove.items.forEach(id => e.hide(id))
})

JEIEvents.hideFluids((e) => {
    global.hide_and_remove.fluids.forEach(id => e.hide(id))
})