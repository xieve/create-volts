// priority: 0

var handler

function make_handler(e, item_entity) {
    return () => {
        if (item_entity.onGround) {
            e.server.runCommandSilent(
                `execute at ${item_entity.uuid} run playsound createvolts:metal_pipe_falling block @a ^ ^ ^ 3`
            )
        } else {
            e.server.scheduleInTicks(1, make_handler(e, item_entity))
        }
    }
}

ItemEvents.dropped("#forge:rods/all_metal", (e) => {
    if (e.item.hasTag("forge:rods/all_metal")) {
        make_handler(e, e.getItemEntity())()
    }
})
