// priority: 100

const IE = (id) => `immersiveengineering:${id}`

/**
 * Assigns all entries from `source` to `target` whose value is not `undefined`
 * @param {any} target
 * @param {{[key: String]: [value: any]}} source
 */
function assign_if_defined(target, source) {
    for (const [key, value] of Object.entries(source)) {
        if (value !== undefined) target[key] = value
    }
}

/**
 * Execute `callback` for every recipe that matches `filter` and replace the original one with the one that
 * `callback` returned
 * @param {Internal.RecipesEventJS} e
 * @param {Internal.RecipeFilter} filter
 * @param {function(Internal.RecipeJS recipe):Internal.RecipeJS} callback
 */
function map_recipes(e, filter, callback) {
    e.forEachRecipe(filter, (recipeJSON) => {
        e.custom(callback(JSON.parse(recipeJSON.json)))
    })
    e.remove(filter)
}

/**
 * Create Create-like recipe
 * @param {Internal.RecipesEventJS} e
 * @param {string} type
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 * @param {string} [heatRequirement]
 * @param {number} [processingTime]
 */
function create_recipe(
    e,
    type,
    ingredients,
    results,
    heatRequirement,
    processingTime
) {
    // note to future self: no, KubeJS does not support shorthand declaration of properties. nor does it support spreads. T-T
    const recipe = {
        type: type,
        ingredients: ingredients,
        results: results,
    }
    assign_if_defined(recipe, {
        heatRequirement: heatRequirement,
        processingTime: processingTime
    })
    e.custom(recipe)
}

/**
 * Create Create compacting recipe.
 * @param {Internal.RecipesEventJS} e
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 * @param {string} [heatRequirement]
 */
function compact(e, ingredients, results, heatRequirement) {
    create_recipe(
        e,
        "create:compacting",
        ingredients,
        results,
        heatRequirement
    )
}

/**
 * Create Create filling recipe.
 * @param {Internal.RecipesEventJS} e
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 */
function spout(e, ingredients, results) {
    create_recipe(e, "create:filling", ingredients, results)
}

/**
 * Create Create filling recipe.
 * @param {Internal.RecipesEventJS} e
 * @param {Array<Object>} ingredients
 * @param {Array<Object>} results
 * @param {number} processingTime
 */
function create_crush(e, ingredients, results, processingTime) {
    const main_result = results[0]
    const secondary_chance = main_result.count % 1
    if (secondary_chance !== 0) {
        results.splice(1, 0, {
            item: main_result.item,
            chance: secondary_chance,
        })
        results[0] = {
            item: main_result.item,
            count: Math.floor(main_result.count),
        }
    }
    create_recipe(
        e,
        "create:crushing",
        ingredients,
        results,
        "",
        processingTime
    )
}

/**
 * Create Immersive Engineering Crusher recipe.
 *
 * `results` are specified like this: `[[1.5, "minecraft:dirt"], [0.25, "minecraft:diamond"]]`. The first item in the
 * `results` array is the main result. It can have any decimal number. Secondary results can have numbers x where 0 < x <= 1.
 * @param {Internal.RecipesEventJS} e
 * @param {string} ingredient
 * @param {Array<Array>} results
 * @param {number} energy
 */
function ie_crush(e, ingredient, results, energy) {
    const recipe = {
        type: IE("crusher"),
        energy: energy,
        input: { item: ingredient },
        result: {},
        secondaries: [],
    }

    const main_result = {
        item: results[0][1],
        count: results[0][0],
        count_floor: Math.floor(results[0][0]),
    }
    if (main_result.count_floor === 1) {
        recipe.result = { item: main_result.item }
    } else {
        recipe.result = {
            base_ingredient: {
                item: main_result.item,
            },
            count: main_result.count_floor,
        }
    }
    let secondary_chance = main_result.count % 1
    if (secondary_chance !== 0) {
        recipe.secondaries.push({
            chance: secondary_chance,
            output: { item: main_result.item },
        })
    }
    if (results.length > 1) {
        results.slice(1).forEach((result) => {
            recipe.secondaries.push({
                chance: result[0],
                output: {
                    item: result[1],
                },
            })
        })
    }
    e.custom(recipe)
}
