// priority: 0

// Make IE Crates drop their contents when broken instead of retaining it
// Adapted from https://pastebin.com/4ucJg0z1

BlockEvents.broken((e) => {
    const forceDropContainers = [
        IE("crate"),
        IE("reinforced_crate"),
    ]
    if (forceDropContainers.includes(e.block.id)) {
        let inventory = e.block.getInventory(Direction.UP) // Direction doesnt matter
        inventory.allItems.forEach((item) => e.block.popItem(item))

        // Destroy old crate and spawn new empty one
        e.block.set("air")
        e.block.popItem(e.block.id)

        e.cancel()
    }
})
