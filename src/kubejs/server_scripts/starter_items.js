// priority: 0

PlayerEvents.loggedIn((e) => {
    if (!e.player.stages.has("starter_items")) {
        e.player.stages.add("starter_items")
        e.player.give("ftbquests:book")
    }
})