dir=$(git rev-parse --show-toplevel)

packwiz () {
    pushd "$dir/src/" \
    && command packwiz "$@" \
    && popd
}
