from pprint import pprint
from re import IGNORECASE, sub
from sys import argv
from json import dump, load


def parse_distrib(entry):
    values = {}
    for [height, abundance] in [s.split(",") for s in entry["distrib"].strip(";").split(";")]:
        values[int(height)] = float(abundance)
    return values


with open("regionscanner world-gen.json") as primary_file, open("jeargh world-gen.json") as secondary_file:
    primary_file = load(primary_file)
    secondary_file = load(secondary_file)
    secondary_file_drops = {}
    for entry in secondary_file:
        secondary_file_drops[entry["block"]] = entry["dropsList"]

    primary_file_dict = {}
    for entry in primary_file:
        primary_file_dict[entry["block"]] = entry

    for block, entry in primary_file_dict.items():
        if not entry["silktouch"]:
            del entry["silktouch"]
        try:
            entry["dropsList"] = secondary_file_drops[block]
        except KeyError:
            pass

        # Merge deepslate variants
        non_deepslate_block = sub(r":deepslate_(.*)ore", r":\1ore", block, flags=IGNORECASE)
        if non_deepslate_block != block:
            print(block, non_deepslate_block)
            non_deepslate_distrib = parse_distrib(primary_file_dict[non_deepslate_block])
            for height, abundance in parse_distrib(entry).items():
                try:
                    non_deepslate_distrib[int(height)] += float(abundance)
                except KeyError:
                    non_deepslate_distrib[int(height)] = float(abundance)
                
            entry["distrib"] = ";".join([",".join(l) for l in [[str(x) for x in pair] for pair in non_deepslate_distrib.items()]])
            primary_file_dict[non_deepslate_block]["distrib"] = entry["distrib"]


with open("world-gen.json", "w") as out_file:
    dump(primary_file, out_file, indent="\t")
